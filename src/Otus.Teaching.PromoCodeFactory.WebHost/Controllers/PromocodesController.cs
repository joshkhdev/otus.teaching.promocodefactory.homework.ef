﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Customer> customersRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync(CancellationToken cancellationToken)
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync(cancellationToken);

            var result = promoCodes.Select(pc => 
                new PromoCodeShortResponse()
                {
                    Id = pc.Id,
                    Code = pc.Code,
                    ServiceInfo = pc.ServiceInfo,
                    BeginDate = pc.BeginDate.ToString(),
                    EndDate = pc.EndDate.ToString(),
                    PartnerName = pc.PartnerName,
                }).ToList();

            return Ok(result);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PromoCodeShortResponse>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request, CancellationToken cancellationToken)
        {
            // Так как в данной работе дается один промокод на одного клиента - выдаем первому, у кого есть такое предпочтение
            
            var customers = await _customersRepository.GetAllAsync(cancellationToken);

            var customerId = customers.FirstOrDefault(c => c.Preferences.Any(p => p.PreferenceId == request.PreferenceId))?.Id;

            if (customerId is null)
            {
                return NotFound();
            }

            var promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                PartnerName = request.PartnerName,
                PartnerManagerId = request.PartnerManagerId,
                CustomerId = customerId.Value,
                PreferenceId = request.PreferenceId,
            };

            var result = await _promoCodeRepository.AddAsync(promoCode, cancellationToken);

            var promoCodeResponse = new PromoCodeShortResponse()
            {
                Id = result.Id,
                Code = result.Code,
                ServiceInfo = result.ServiceInfo,
                BeginDate = result.BeginDate.ToString(),
                EndDate = result.EndDate.ToString(),
                PartnerName = result.PartnerName,
            };

            return Ok(promoCodeResponse);            
        }
    }
}