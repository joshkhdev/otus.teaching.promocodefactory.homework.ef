﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync(CancellationToken cancellationToken)
        {
            var employees = await _employeeRepository.GetAllAsync(cancellationToken);

            var employeesModelList = employees.Select(e =>
                new EmployeeShortResponse()
                {
                    Id = e.Id,
                    FullName = e.FullName,
                    Email = e.Email,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var employee = await _employeeRepository.GetByIdAsync(id, cancellationToken);

            if (employee == null)
            {
                return NotFound();
            }

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.Role.Id,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description,
                },
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody] CreateEmployeeRequest createRequest, CancellationToken cancellationToken)
        {
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = createRequest.FirstName,
                LastName = createRequest.LastName,
                Email = createRequest.Email,
                RoleId = createRequest.RoleId,
                AppliedPromocodesCount = 0,
            };

            var result = await _employeeRepository.AddAsync(employee, cancellationToken);

            var employeeModel = new EmployeeResponse()
            {
                Id = result.Id,
                FullName = result.FullName,
                Email = result.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.Role.Id,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description,
                },
                AppliedPromocodesCount = result.AppliedPromocodesCount,
            };

            return employeeModel;
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, [FromBody] UpdateEmployeeRequest updateRequest, CancellationToken cancellationToken)
        {
            var employee = new Employee()
            {
                Id = id,
                FirstName = updateRequest.FirstName,
                LastName = updateRequest.LastName,
                Email = updateRequest.Email,
                RoleId = updateRequest.RoleId,
                AppliedPromocodesCount = updateRequest.AppliedPromocodesCount,
            };

            var success = await _employeeRepository.UpdateAsync(employee, cancellationToken);

            if (!success)
            {
                return NotFound();
            }

            return Ok(success);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id, CancellationToken cancellationToken)
        {
            var success = await _employeeRepository.DeleteAsync(id, cancellationToken);

            if (!success)
            {
                return NotFound();
            }

            return Ok(success);
        }
    }
}