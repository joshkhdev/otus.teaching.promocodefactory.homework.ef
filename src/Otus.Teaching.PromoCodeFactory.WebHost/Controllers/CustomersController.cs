﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync(CancellationToken cancellationToken)
        {
            var customers = await _customerRepository.GetAllAsync(cancellationToken);

            var result = customers.Select(c => new CustomerShortResponse()
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Email = c.Email,
            }).ToList();

            return Ok(result);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.GetByIdAsync(id, cancellationToken);

            if (customer is null)
            {
                return NotFound();
            }

            var result = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes.Select(pc =>
                    new PromoCodeShortResponse()
                    {
                        Id = pc.Id,
                        Code = pc.Code,
                        ServiceInfo = pc.ServiceInfo,
                        BeginDate = pc.BeginDate.ToString(),
                        EndDate = pc.EndDate.ToString(),
                        PartnerName = pc.PartnerName,
                    }).ToList(),
                Preferences = customer.Preferences.Select(p =>
                    new PreferenceResponse()
                    {
                        Id = p.Preference.Id,
                        Name = p.Preference.Name,
                    }).ToList(),
            };

            return Ok(result);
        }

        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request, CancellationToken cancellationToken)
        {
            var preferences = await _preferenceRepository.GetAllAsync(cancellationToken);

            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            var customerPreferences = request.PreferenceIds.Select(pId =>
                new CustomerPreference()
                {
                    PreferenceId = pId,
                    Preference = preferences.Find(p => p.Id == pId),
                    CustomerId = customer.Id,
                    Customer = customer,
                }).ToList();
            customer.Preferences = customerPreferences;

            var result = await _customerRepository.AddAsync(customer, cancellationToken);
            var createdCustomer = new CustomerResponse()
            {
                Id = result.Id,
                FirstName = result.FirstName,
                LastName = result.LastName,
                Email = result.Email,
                PromoCodes = result.PromoCodes.Select(pc =>
                    new PromoCodeShortResponse()
                    {
                        Id = pc.Id,
                        Code = pc.Code,
                        ServiceInfo = pc.ServiceInfo,
                        BeginDate = pc.BeginDate.ToString(),
                        EndDate = pc.EndDate.ToString(),
                        PartnerName = pc.PartnerName,
                    }).ToList(),
                Preferences = result.Preferences.Select(p =>
                    new PreferenceResponse()
                    {
                        Id = p.Preference.Id,
                        Name = p.Preference.Name,
                    }).ToList(),
            };

            return Ok(createdCustomer);
        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request, CancellationToken cancellationToken)
        {
            var preferences = await _preferenceRepository.GetAllAsync(cancellationToken);

            var customer = new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            var customerPreferences = request.PreferenceIds.Select(pId =>
                new CustomerPreference()
                {
                    Preference = preferences.Find(p => p.Id == pId),
                    Customer = customer,
                }).ToList();
            customer.Preferences = customerPreferences;

            var success = await _customerRepository.UpdateAsync(customer, cancellationToken);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id, CancellationToken cancellationToken)
        {
            var success = await _customerRepository.DeleteAsync(id, cancellationToken);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}