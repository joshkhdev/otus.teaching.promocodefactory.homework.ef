﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string PromoCode { get; set; }

        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public Guid PreferenceId { get; set; }

        public Guid PartnerManagerId { get; set; }
    }
}