﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DatabaseContext Context;

        private readonly DbSet<T> _entitySet;

        public EfRepository(DatabaseContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        public async Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _entitySet.FirstOrDefaultAsync(e => e.Id == id, cancellationToken);
        }

        public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _entitySet.ToListAsync(cancellationToken);
        }

        public async Task<T> AddAsync(T entity, CancellationToken cancellationToken)
        {
            var result = (await _entitySet.AddAsync(entity)).Entity;
            await Context.SaveChangesAsync();

            return result;
        }

        public async Task<bool> UpdateAsync(T entity, CancellationToken cancellationToken)
        {
            var entityExists = await _entitySet.AnyAsync(e => e.Id == entity.Id);

            if (!entityExists)
            {
                return false;                
            }

            _entitySet.Update(entity);
            await Context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var entity = await _entitySet.FirstOrDefaultAsync(e => e.Id == id);

            if (entity is null)
            {
                return false;
            }

            _entitySet.Remove(entity);
            await Context.SaveChangesAsync();

            return true;
        }
    }
}
