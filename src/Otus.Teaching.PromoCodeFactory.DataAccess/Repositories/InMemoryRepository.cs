﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }
        
        public Task<List<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T entity, CancellationToken cancellationToken)
        {
            Data.Add(entity);

            return Task.FromResult(entity);
        }

        public Task<bool> UpdateAsync(T entity, CancellationToken cancellationToken)
        {            
            var entityIndex = Data.FindIndex(e => e.Id == entity.Id);
            
            // If not found
            if (entityIndex == -1)
            {
                return Task.FromResult(false);
            }

            Data[entityIndex] = entity;

            return Task.FromResult(true);
        }

        public Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var entityIndex = Data.FindIndex(e => e.Id == id);

            // If not found
            if (entityIndex == -1)
            {
                return Task.FromResult(false);
            }

            Data.RemoveAt(entityIndex);

            return Task.FromResult(true);
        }
    }
}