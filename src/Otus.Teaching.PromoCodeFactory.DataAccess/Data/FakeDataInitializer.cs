﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class FakeDataInitializer : DbInitializer
    {
        public DatabaseContext _context { get; set; }

        public FakeDataInitializer(DatabaseContext context)
        {
            _context = context;
        }

        public void Initialize()
        {
            //_context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();

            //_context.AddRange(FakeDataFactory.Roles);
            //_context.AddRange(FakeDataFactory.Employees);
            //_context.AddRange(FakeDataFactory.Preferences);
            //_context.AddRange(FakeDataFactory.Customers);
            //_context.AddRange(FakeDataFactory.PromoCodes);

            //_context.SaveChanges();
        }
    }
}
