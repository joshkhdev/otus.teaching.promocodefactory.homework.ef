﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface DbInitializer
    {
        public void Initialize();
    }
}
