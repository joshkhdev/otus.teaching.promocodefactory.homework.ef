﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
        };

        public static List<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            },
            new Preference()
            {
                Id = Guid.Parse("2e5e87ce-e033-47f5-b857-7674a9dcdc6b"),
                Name = "Роботы",
            },
            new Preference()
            {
                Id = Guid.Parse("f61acd8c-ddc1-4a5e-a15e-250ef7cf7992"),
                Name = "Спорт",
            },
        };

        public static List<Customer> Customers
        {
            get
            {
                var ivanId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var borisId = Guid.Parse("65700514-0027-4cde-86fe-7654a2932b66");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_petrov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<CustomerPreference>
                        {
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Театр").Id,
                                CustomerId = ivanId,
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Роботы").Id,
                                CustomerId = ivanId,
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Спорт").Id,
                                CustomerId = ivanId,
                            },
                        },
                    },
                    new Customer()
                    {
                        Id = borisId,
                        Email = "boris_sergeev@mail.ru",
                        FirstName = "Борис",
                        LastName = "Сергеев",
                        Preferences = new List<CustomerPreference>
                        {
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Семья").Id,
                                CustomerId = borisId,
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Дети").Id,
                                CustomerId = borisId,
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Find(p => p.Name == "Спорт").Id,
                                CustomerId = borisId,
                            },
                        },
                    },
                };

                return customers;
            }
        }

        public static List<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "THEATRE10",
                        ServiceInfo = "ИндексРеклама",
                        BeginDate = DateTime.Now.AddDays(-10),
                        EndDate = DateTime.Now.AddDays(20),
                        PartnerName = "Большой театр",
                        PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                        PreferenceId = Preferences.Find(p => p.Name == "Театр").Id,
                        CustomerId = Customers.Find(c => c.FirstName == "Иван").Id,
                    },
                    new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "GYM2024",
                        ServiceInfo = "ToggleAds",
                        BeginDate = DateTime.Now.AddDays(-100),
                        EndDate = DateTime.Now.AddDays(100),
                        PartnerName = "ЖелезоРядом",
                        PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                        PreferenceId = Preferences.Find(p => p.Name == "Спорт").Id,
                        CustomerId = Customers.Find(c => c.FirstName == "Борис").Id,
                    },
                };

                return promoCodes;
            }
        }
    }
}