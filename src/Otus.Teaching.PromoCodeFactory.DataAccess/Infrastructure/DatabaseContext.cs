﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Infrastructure
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Role>(entity =>
            {
                entity.HasKey(r => r.Id);
                entity.Property(r => r.Name).HasMaxLength(100).IsRequired();
                entity.Property(r => r.Description).HasMaxLength(250);
            });

            builder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.FirstName).HasMaxLength(100).IsRequired();
                entity.Property(e => e.LastName).HasMaxLength(100).IsRequired();
                entity.Property(e => e.Email).HasMaxLength(250).IsRequired();
                entity.Property(e => e.AppliedPromocodesCount).HasDefaultValue(0);

                entity
                    .HasOne(e => e.Role)
                    .WithMany()
                    .HasForeignKey(e => e.RoleId)
                    .IsRequired();
            });

            builder.Entity<Preference>(entity =>
            {
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Name).HasMaxLength(250).IsRequired();
            });

            builder.Entity<Customer>(entity =>
            {
                entity.HasKey(c => c.Id);
                entity.Property(c => c.FirstName).HasMaxLength(100).IsRequired();
                entity.Property(c => c.LastName).HasMaxLength(100).IsRequired();
                entity.Property(c => c.Email).HasMaxLength(250).IsRequired();

                entity
                    .HasMany(c => c.PromoCodes)
                    .WithOne()
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<PromoCode>(entity =>
            {
                entity.HasKey(pc => pc.Id);
                entity.Property(pc => pc.Code).HasMaxLength(100).IsRequired();
                entity.Property(pc => pc.ServiceInfo).HasMaxLength(250).IsRequired();
                entity.Property(pc => pc.PartnerName).HasMaxLength(100).IsRequired();
                entity.Property(pc => pc.BeginDate).IsRequired();
                entity.Property(pc => pc.EndDate).IsRequired();

                entity
                    .HasOne(pc => pc.PartnerManager)
                    .WithMany()
                    .HasForeignKey(pc => pc.PartnerManagerId);

                entity
                    .HasOne(pc => pc.Customer)
                    .WithMany(c => c.PromoCodes)
                    .HasForeignKey(pc => pc.CustomerId);

                entity
                    .HasOne(pc => pc.Preference)
                    .WithMany()
                    .IsRequired();
            });

            builder.Entity<CustomerPreference>(entity =>
            {
                entity.HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

                entity
                    .HasOne(cp => cp.Customer)
                    .WithMany(c => c.Preferences)
                    .HasForeignKey(cp => cp.CustomerId);

                entity
                    .HasOne(cp => cp.Preference)
                    .WithMany()
                    .HasForeignKey(cp => cp.PreferenceId);
            });
        }
    }
}
