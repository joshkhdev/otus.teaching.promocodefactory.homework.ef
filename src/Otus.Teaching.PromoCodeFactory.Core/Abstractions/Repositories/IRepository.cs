﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> where T: BaseEntity
    {
        Task<List<T>> GetAllAsync(CancellationToken cancellationToken);
        
        Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<T> AddAsync(T entity,CancellationToken cancellationToken);

        Task<bool> UpdateAsync(T entity, CancellationToken cancellationToken);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}